import * as React from 'react';
import './App.css';
import { connect, DispatchProp } from 'react-redux'
import { getTracks } from "./actions/tracks";

interface IProps extends DispatchProp {
  tracks: any;
}

class App extends React.Component<IProps> {

  private trackInput: any;
  private searchInput: any;

  addTrack() {
    console.log('addTrack', this.trackInput);

    this.props.dispatch({
      type: 'ADD_TRACK',
      payload: {
        id: Date.now().toString(),
        name: this.trackInput.value
      }
    });

    this.trackInput.value = '';
  }

  findTrack() {
    console.log('findTrack', this.searchInput.value);

    this.props.dispatch({
      type: 'FIND_TRACK',
      payload: this.searchInput.value
    });
  }

  onGetTracks() {
    this.props.dispatch(getTracks());
  }

  public render() {
    console.log(this.props.tracks);
    return (
      <div>
        <div>
          <input type="text" ref={(input) => {
            this.trackInput = input;
          }}/>
          <button onClick={this.addTrack.bind(this)}>Add track</button>
        </div>
        <div>
          <input type="text" ref={(input) => {
            this.searchInput = input;
          }}/>
          <button onClick={this.findTrack.bind(this)}>Find track</button>
        </div>
        <div>
          <button onClick={this.onGetTracks.bind(this)}>Get tracks</button>
        </div>

        <ul>
          {this.props.tracks.map((track: any, index: any) =>
            <li key={index}>{track.name}</li>
          )}
        </ul>
      </div>
    );
  }
}

export const AppConnect = connect(
  (state: any) => ({
    tracks: state.tracks.filter((track: any) => track.name.includes(state.filterTracks))
  })
)(App);