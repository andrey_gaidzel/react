import { combineReducers } from 'redux';

import tracks from './tracks';
import playlists from './playlists';
import filterTracks from "./filterTracks";

export const combReducers = combineReducers({
  tracks,
  playlists,
  filterTracks
})