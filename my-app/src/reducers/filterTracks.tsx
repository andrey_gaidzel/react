const initialState: any = '';

export default function filterTracks(state = initialState, action: any) {
  if (action.type === 'FIND_TRACK') {
    return action.payload;
  }
  return state;
}