import {createStore} from 'redux';

// export const t = 123;

function playlist(state = [], action: any) {
    if (action.type === 'ADD_TRACK') {
        return [
            ...state,
            action.payload
        ]
    }
    return state;
}

const store = createStore(playlist);
const trackInput = (document.querySelector('.trackInput') as HTMLInputElement);
const list = document.querySelectorAll('.list')[0];
const addTrackBtn = document.querySelectorAll('.addTrack')[0];

store.subscribe(() => {
    console.log(store.getState());
    list.innerHTML = '';
    trackInput.value = '';
    store.getState().forEach(track => {
        const li = document.createElement('li');
        li.textContent = track;
        list.appendChild(li);
    });
});

addTrackBtn.addEventListener('click', () => {
    store.dispatch({type: 'ADD_TRACK', payload: trackInput.value});
});