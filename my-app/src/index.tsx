import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { AppConnect } from "./App";
import { Provider } from 'react-redux'
import { composeWithDevTools } from "redux-devtools-extension";
import { combReducers } from "./reducers";
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const store = () => {
  return createStore(
    combReducers,
    composeWithDevTools(applyMiddleware(thunk))
  )
};

ReactDOM.render(
  <Provider store={store()}>
    <AppConnect/>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();


